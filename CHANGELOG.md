## CHANGELOG

### v1.1
- Tested `go program`
- Tested `index.html`

### v1.0
- Refactored `go code`
- Refactored `index.html`
