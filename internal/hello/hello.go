package hello

import (
	"fmt"
)

// Greet says hello to a name or to world!
func Greet(name string) string {
	if name == "" {
		name = "test world"
	}
	return fmt.Sprintf("Hello, %s!", name)
}
