package hello

import "testing"

func TestGreet(t *testing.T) {
	tests := []struct {
		name string
		in   string
		want string
	}{
		{
			name: "with_name",
			in:   "BMPO",
			want: "Hello, BMPO!",
		},
		{
			name: "without_name",
			in:   "",
			want: "Hello, test world!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greet(tt.in); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}
