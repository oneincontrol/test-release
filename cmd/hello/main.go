package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/oneincontrol/test-release/internal/hello"
)

func main() {
	// Use flag package for better argument parsing and help message
	name := flag.String("name", "World", "the name to greet")
	flag.Parse()

	// Validate the input, provide help message if invalid
	if *name == "" {
		fmt.Println("Please provide a valid name using the -name flag.")
		os.Exit(1)
	}

	// Call the greet function from the hello package
	fmt.Println(hello.Greet(*name))
}
